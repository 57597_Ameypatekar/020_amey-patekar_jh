/*SECTION I*/
/*Q.1*/

create table DEPT (
DEPTNO int(2),
DNAME varchar(15),
LOC varchar(10)
);

insert into DEPT values 
(10, 'ACCOUNTING', 'NEW YORK'),
(20, 'RESEARCH', 'DALLAS'),
(30, 'SALES', 'CHICAGO'),
(40, 'OPERATIONS', 'BOSTON');

select * from dept;
/*===================================================================================================*/
/*Q.2*/
create table EMP (
EMPNO int(4),
ENAME varchar(10),
JOB varchar(9),
HIREDATE date,
SAL float(7,2),
COMM float(7,2),
DEPTNO int(2)
);

insert into EMP values 
(7839, 'KING', 'MANAGER', '1991-11-17', 5000, NULL, 10),
(7698, 'BLAKE', 'CLERK', '1981-05-01', 2850, NULL, 30),
(7782, 'CLARK', 'MANAGER', '1981-06-09', 2450, NULL, 10),
(7566, 'JONES', 'CLERK', '1981-04-02', 2975, NULL, 20),
(7654, 'MARTIN', 'SALESMAN', '1981-09-28', 1250, 1400, 30),
(7499, 'ALLEN', 'SALESMAN', '1981-02-20', 1600, 300, 30);

select * from emp;

/*===================================================================================================*/
/*3. Display all the employees where SAL between 2500 and 5000 (inclusive of both).*/

	select * from emp where sal between 2500 and 5000;

/*===================================================================================================*/
/*4. Display all the ENAMEs in descending order of ENAME.*/

	select ename from emp order by ename desc;

/*===================================================================================================*/

/*5. Display all the JOBs in lowercase.*/

	select lower(job) from emp ;

/*===================================================================================================*/

/*6. Display the ENAMEs and the lengths of the ENAMEs.*/

	select ename, length(ename) "Length" from emp;

/*===================================================================================================*/

/*7. Display the DEPTNO and the count of employees who belong to that DEPTNO .*/

	select dept.deptno,count(emp.ename) "Count of Employees" from emp,dept
	where emp.deptno=dept.deptno 
	group by deptno ;


/*===================================================================================================*/

/*8. Display the DNAMEs and the ENAMEs who belong to that DNAME.*/

	select dname, ename from emp, dept
	where dept.deptno = emp.deptno 
	order by 1;


/*===================================================================================================*/
/*9. Display the position at which the string ‘AR’ occurs in the ename.*/

	select instr(ename,'ar') "Position of AR" from emp;


/*===================================================================================================*/

/*10. Display the HRA for each employee given that HRA is 20% of SAL.*/

	select ename, sal*0.20 "HRA" from emp;


/*===================================================================================================*/
/*SECTION II */
/*Q.1*/

create table tempp (
string1 varchar(20),
string2 varchar(20),
status varchar(10)
);

delimiter //
create procedure PROC1 (str1 varchar(20), str2 varchar(20))
begin
	declare x int ;
    set x = locate(str1, str2);
    if x <> 0 then 
		insert into tempp values (str1, str2, 'PRESENT') ;
    else
		insert into tempp values (str1, str2, 'NOT PRESENT') ;
	end if ;
end ; //
delimiter ;

call PROC1 ('DAC','CDAC') ;
select * from tempp;

/*===================================================================================================*/
/*SECTION II */
/*Q.2*/

create table temp(
side1 float, side2 float, side3 float, status varchar(20)
);

delimiter //
create function FUNC1 (s1 float, s2 float, s3 float)
returns boolean
deterministic
begin
		if (s1+s2>s3) and (s1+s3>s2) and (s2+s3>s1) then
			return TRUE ;
		else 
			return FALSE ;
		end if ;
end ; //
delimiter ;

delimiter //
create procedure valid_triangle (s1 float, s2 float, s3 float)
begin	
	declare x boolean ;
    set x = FUNC1 (s1 , s2 , s3 );
    if x = true then
		insert into temp values (s1,s2,s3,'VALILD TRIANGLE') ;
    else 
		insert into temp values (s1,s2,s3,'INVALILD TRIANGLE') ;
    end if;
end ; //
delimiter ;

call valid_triangle(10,7,25);
call valid_triangle(5,5,5);
select * from temp;

/*===================================================================================================*/