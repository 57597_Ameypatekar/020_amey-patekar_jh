//Q.3 Two Stack in single array

class Stack{
	int n=10;
	int arr[]=new int[n];
	int top1=-1;
	int top2=-1;
	
	public void push1(int data){
		
		if(top1==-1)
		{	
			top1=1;
			arr[top1--]=data;
		}
		else if(top1==-1)
		{
			return;
		}
		else{
			arr[top1--]=data;
		}
	}
	
	public void push2(int data){
		/*for(int i=6;i<10;i++)
			arr[i]=data;*/
		if(top2==-1)
		{	
			top2=6;
			arr[top2++]=data;
		}
		else if(top2==10)
		{
			return;
		}
		else{
			arr[top2++]=data;
		}
	}
	
	public void display(){
		for(int i=0;i<10;i++)
			System.out.print(arr[i]+" ");
	}
	public void pop1(){
		System.out.println("\n\tPopped element from stack1 is "+arr[top1+1]+"\n");
		if(top1>0)
		{top1--;}
	}
	
	public void pop2(){
		System.out.println("\n\tPopped element from stack2 is "+arr[top2-1]+"\n");
		top2--;
	}

	public static void main(String []args){
		
		
		Stack s=new Stack();
		s.push1(5);
		s.push2(10);
		s.push2(15);
		s.push1(11);
		s.push2(7);
		s.push2(40);
		
		s.pop1();
		s.pop2();
	}
}