//Q.2 Reverse a linked list
import java.util.Scanner;
class ReverseLinkedList{
	class Node{
	int data;
	Node next;
	
	public Node(int data){
		this.data=data;
		this.next=null;
	}
	
	}
	
	Node head=null;
	Node tail=null;
	public void addNode(int data){
		Node newNode=new Node(data);
		
		if(head==null)
		{
			head=tail=newNode;
		}
		else{
			
			tail.next=newNode;
			tail=newNode;
		}
	}
	
	public void display(){
		Node trav=head;
		if(head==null)
		{
			System.out.println("\tThe linked list is empty");
		}
		else{
			System.out.print("\t");
			while(trav!=null)
			{
				System.out.print(trav.data+" ");
				trav=trav.next;
			}
			
		}
		
	}
	
	public void reverse(){
		Node prevnode=null;
		Node nextnode=null;
		Node trav=null;
		
		trav=nextnode=head;
		while(trav!=null)
		{
			nextnode=nextnode.next;
			trav.next=prevnode;
			prevnode=trav;
			trav=nextnode;
		}
		head=prevnode;
		display();
		
	}
	
	public void deleteAll(){
		head=null;
	}
	
	public static void main(String []args){
		ReverseLinkedList l=new ReverseLinkedList();
		Scanner sc=new Scanner(System.in);
		int ch=1;
		while (ch==1)
		{
			System.out.println("\n");
			System.out.print("\t1.Testcase\t2.Testcase\t0.Exit\n\tEnter choice : \n\t");
			int choice=sc.nextInt();
			switch(choice)
			{
				case 1:System.out.print("\t");
							int n=sc.nextInt();
						
						for(int i=0;i<n;i++)
						{
							System.out.print("\t");
							int value=0;
							value=sc.nextInt();
							l.addNode(value);
						}
						//l.display();
						l.reverse();
						l.deleteAll();
						break;
				case 2:System.out.print("\t");
							int n1=sc.nextInt();
						
						for(int i=0;i<n1;i++)
						{
							System.out.print("\t");
							int value=0;
							value=sc.nextInt();
							l.addNode(value);
						}
						//l.display();
						l.reverse();
						
						break;
				case 0:ch=0;
						break;
			}
			
		}
		
	}
}