//Q.1 Sorting
import java.util.Scanner;
class Sorting{

	public static void insertionSort(int arr[]){
		int j=0,temp=0;
		for(int i=4;i>-1;i--)
		{
			temp=arr[i];
			j=i-1;
			while(j>0 && arr[j]>temp)
			{
				arr[j+1]=arr[j];
				System.out.print("\t");
				for(int k=0;k<5;k++)
				{
					System.out.print(arr[k]+" ");
				}
				System.out.println();
				j--;
				
			}
			
			arr[j+1]=temp;
		}
	}
	
	public static void main(String []args){
	Scanner sc=new Scanner(System.in);
	System.out.println("\tTest case 1 : ");
	System.out.print("\t");
	int n=sc.nextInt();
	int arr[]=new int[n];
	
	for(int i=0;i<arr.length;i++)
	{
		System.out.print("\t");
		arr[i]=sc.nextInt();
	}
	
	
	System.out.println();
	insertionSort(arr);
	
	//displaying array elements after sorting
	System.out.print("\t");
	for(int i=0;i<arr.length;i++)
		System.out.print(arr[i]+" ");
	}
}